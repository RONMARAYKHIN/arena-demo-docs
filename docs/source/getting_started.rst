 
================
Getting Started
================

.. contents:: Table of Contents
    :local:
    
Launching Arena
---------------

To launch Arena from terminal run:
    
    .. code-block:: console

        arena


To launch Arena and host specific level use the following syntax:
    
    .. code-block:: console

        arena -host=<user_name>,<level_name>,<latitude>,<longitude>,<altitude_amsl>,<time_of_day>,<use_oem_buildings>

for example:

    .. code-block:: console

        arena -host=john,CityPark,31.363789,34.877132,442.5,13,true


To Launch Arena in headless mode add the -RenderOffscreen argument, e.g.
    
    .. code-block:: console

        arena -RenderOffscreen -host=john,CityPark,31.363789,34.877132,442.5,13,true


See Host menu section for more details.


It is also possible to launch Arena from the grphical Application Launcher.


Menus
-----


Main menu
```````````

TODO: add image

From the main menu you can set the user name that will be used for both hosting a new simulation and for joining an existing session.

Keep in mind that joining an existing session requires a unique user name.


Host menu
`````````````

TODO: add image


This menu is used to select and set the level (envirenment) parameters.

The server name is used as identification name for multi user scenarios.

The geographic coordinates are used to set the absolute world position of the cartesian origin of the level.

Time of day is given as a decimal number between 0 to 24.

Join menu
`````````````

TODO: add image


Entering this menu trigger a scan for available servers running ont the same LAN.

Clicking on a server name will initiate joining the server (*).

Hot Keys
--------

After hosting ir joining a server, you can use the following keys for exploring, debugging ir manually controlling the simulation:

.. list-table:: Hot Keys
   :widths: 25 100
   :header-rows: 1

   * - Key
     - Function
   * - F1
     - Toggle Hot Keys help view on screen
   * - Escape
     - Quit Arena
   * - Ctrl+Q
     - Quit level (back to main menu)
   * - A,W,S,D,Q,E
     - Camera view adjustment keys (can be used with mouse click and drag)
   * - K
     - Toggle Spawner widget (*)
   * - P
     - Toggle Profiling widget (*)
   * - O
     - Toggle Scalability widget (graphicas performance adjustments)
   * - T
     - Toggle Envirenment settings widget
   * - L
     - Toggle robots widget (top left of the screen)
   * - N
     - Toggle robot head name view (some robots include additional properties view)
   * - Arrows
     - Robots manual controls


(*) Not available in all demo versions


Widgets
--------

Scalability Widget
`````````````````````

Envirenment Settings Widget
`````````````````````````````

Robots Widget
``````````````````


Comming soon...