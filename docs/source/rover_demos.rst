 
================
Rover Demos
================

.. contents:: Table of Contents
    :local:



Waching te front camera video stream
-------------------------------------------

See Drone Demos page for video streaming and watching examples.

The Rover launch file is rover.launch.py and streamer port is 6000+instance.

For example, to launch Arena with a Rover robot, run:

    .. code-block:: console

        ros2 launch arena_backend_demo rover.launch.py arena:=true


To view the video stream using Gstreamer:

    .. code-block:: console

        gst-launch-1.0 -v tcpclientsrc host=127.0.0.1 port=6001 ! gdpdepay ! videoconvert ! autovideosink
        





Control Rover by publishing Twist messages
-------------------------------------------


#. Launch Arena + Rover:
    
    .. code-block:: console

        ros2 launch arena_backend_demo drone.launch.py arena:=true


#. Publish Twist messages:

    .. code-block:: console

        ros2 topic pub /Rover001/cmd_vel geometry_msgs/msg/Twist "{linear: {x: 1.0, y: 0.0, z: 0.0}, angular: {x: 0.0, y: 0.0, z: 0.8}}"

The above command drives the robot in forward velocity of 1 m/s and angular velocity of 0.8 rad/s. The robot will continue to follow this command untill new command will be snt.


Control Rover from rqt Robot Steering
-------------------------------------


#. Launch Arena + Rover:
    
    .. code-block:: console

        ros2 launch arena_backend_demo drone.launch.py arena:=true


#. launch rqt_robot_steering and set to topic at the top to "/Rover001/cmd_vel"
    
    .. code-block:: console

        ros2 run rqt_robot_steering rqt_robot_steering

Now you can play with the linear and angular velocity sliders.




Running the SLAM demo from the slam_toolbox package
------------------------------------------------------

#. Install `slam_toolbox <https://github.com/SteveMacenski/slam_toolbox>`_:

    .. code-block:: console

        sudo apt install ros-foxy-slam-toolbox


#. Launch Arena + Rover + SLAM demo:

    .. code-block:: console

        ros2 launch arena_backend_demo rover_slam_demo.launch.py

Now you can drive the robot and watch the map build on rviz.



Control Rover using ROS2 navigation2 stack
------------------------------------------

Comming soon...