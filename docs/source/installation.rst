 
================
Installation
================

.. contents:: Table of Contents
    :local:
    
System Requirements
----------------------

* We support Focal Fossa (20.04) on 64-bit x86.
* Nvidia graphics card with installed drivers is required.
* Minimum 16GB RAM
* Free disk storage > 6GB



Installing ROS2
------------------
Please follow the official `ROS2 installation instructions  <https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Debians.html>`_.


Installing Arena
-------------------------

#. Clone the Arena Demo repository using given username and deploy_token
    
    .. code-block:: console

        git clone https://<username>:<deploy_token>@gitlab.com/robotican/arena-simulation/arena-demo.git --depth 1

#. Run installation script
    
    .. code-block:: console

        cd arena-demo
        ./install.sh

    This script install required libraries and packages and the Arena software.

#. Setup licence by adding the following line to your ~/.bashrc file
    
    .. code-block:: console

        export ARENA_LICENSE_FILE_PATH=<PATH_TO_LICENCE_FILE>

    where <PATH_TO_LICENCE_FILE> is the actual absolute path to your .bin license file



Installing Arena Backend Demo Package
---------------------------------------

#. Create new ROS2 workspace (or use an existing one)
    
    .. code-block:: console

        mkdir -p ~/arena_demo_ws/src

#. Extract arena_backend_demo.tar.gz to your ROS2 workspace src folder (e.g ~/arena_demo_ws/src)
    
    .. code-block:: console

        tar -xvf arena_backend_demo.tar.xz -C ~/arena_demo_ws/src

#. Install PX4 SITL and MAVSDK:
    
    .. code-block:: console

        cd ~/arena_demo_ws/src/arena_backend_demo/scripts
        sudo dpkg -i libmavsdk-dev_1.4.7_ubuntu20.04_amd64.deb
        ./install_sitl.sh


#. Build workspace (make sure /opt/ros/foxy/setup.bash is sourced manually or from ~/.bashrc before this step)
    
    .. code-block:: console

            cd ~/arena_demo_ws
            colcon build --symlink-install

#. Source the new workspace
    
    .. code-block:: console

        source ~/arena_demo_ws/install/setup.bash

    Consider adding the above command to your ~/.bashrc to avoid running it manually on each new terminal.