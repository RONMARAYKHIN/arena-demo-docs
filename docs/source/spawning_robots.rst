 
================
Spawning Robots
================

.. contents:: Table of Contents
    :local:

Common spawn options 
----------------------


.. list-table:: Common Spawn Options
   :widths: 1 1 1 1
   :header-rows: 1

   * - Option
     - Notes
     - Range
     - Default
   * - possess
     - Auto possess in Arena after spawn
     - true \\ false
     - true
   * - segmentation_id
     - Set a unique grayscale value for ground truth segmentation
     - 0 - 255
     - 255
   * - use_tf_prefix
     - Enable adding robot name as prefix to all tf frames
     - true \\ false
     - false


Sensors parameters spawn options
----------------------------------

Day Camera Spawn Options
`````````````````````````

.. list-table:: Day Camera Spawn Options
   :widths: 1 1 1 1
   :header-rows: 1

   * - Option
     - Notes
     - Range
     - Default
   * - <sensor_name>_port
     - Port used for gstreamer TCP server
     - any
     - 0 = disable streaming
   * - <sensor_name>_hfov
     - Camera Horizontal field of view in degrees
     - 0.1- 170.0
     - 110
   * - <sensor_name>_fps
     - Camera framerate in Hz
     - 0.1 - 60.0
     - 30
   * - <sensor_name>_width
     - Camera resolution width in pixels
     - 1 - 3840
     - 640
   * - <sensor_name>_height
     - Camera resolution height in pixels
     - 1 - 2160
     - 480
   * - <sensor_name>_gamma
     - Camera gamma value
     - 0.1 - 50
     - 2.2


2D LIDAR Spawn Options
`````````````````````````

.. list-table:: 2D LIDAR Spawn Options
   :widths: 1 1 1 1
   :header-rows: 1

   * - Option
     - Notes
     - Range
     - Default
   * - <sensor_name>_topic
     - Published topic name
     - valid topic name
     - ~/scan
   * - <sensor_name>_scan_rate
     - Published readings rate in Hz
     - 0.1 - 100.0
     - 10.0
   * - <sensor_name>_max_range
     - Sensor maximum range in meters
     - 0.01 - 1000.0
     - 150.0
   * - <sensor_name>_min_range
     - Sensor minimum range in meters
     - 0.01 - 1000.0
     - 0.1
   * - <sensor_name>_max_horizontal_angle
     - Sensor maximum horizontal angle in degrees, clockwise positive
     - -180.0 - 180.0
     - 180.0
   * - <sensor_name>_min_horizontal_angle
     - Sensor minimum horizontal angle in degrees, clockwise positive
     - -180.0 - 180.0
     - -180.0
   * - <sensor_name>_angular_resolution
     - Sensor angular resolution in degrees
     - 0.01 - 180.0
     - 0.1
   * - <sensor_name>_range_noise_stdev
     - Sensor range readings noise standard deviation in meters
     - 0.0 - 100.0
     - 0.0
   * - <sensor_name>_range_noise_bias
     - Sensor range readings noise bias in meters
     - 0.0 - 100.0
     - 0.0



3D (Multi Layer) LIDAR Spawn Options
```````````````````````````````````````

.. list-table:: 3D LIDAR Spawn Options
   :widths: 1 1 1 1
   :header-rows: 1

   * - Option
     - Notes
     - Range
     - Default
   * - <sensor_name>_topic
     - Published topic name
     - valid topic name
     - ~/cloud
   * - <sensor_name>_scan_rate
     - Published readings rate in Hz
     - 0.1 - 100.0
     - 10.0
   * - <sensor_name>_max_range
     - Sensor maximum range in meters
     - 0.01 - 1000.0
     - 20.0
   * - <sensor_name>_min_range
     - Sensor minimum range in meters
     - 0.01 - 1000.0
     - 1.0
   * - <sensor_name>_max_horizontal_angle
     - Sensor maximum horizontal angle in degrees, clockwise positive
     - -180.0 - 180.0
     - 60.0
   * - <sensor_name>_min_horizontal_angle
     - Sensor minimum horizontal angle in degrees, clockwise positive
     - -180.0 - 180.0
     - -60.0
   * - <sensor_name>_angular_resolution
     - Sensor horizontal angular resolution in degrees
     - 0.01 - 180.0
     - 1.0
   * - <sensor_name>_range_noise_stdev
     - Sensor range readings noise standard deviation in meters
     - 0.0 - 100.0
     - 0.01
   * - <sensor_name>_range_noise_bias
     - Sensor range readings noise bias in meters
     - 0.0 - 100.0
     - 0.0
   * - <sensor_name>_max_vertical_angle
     - Sensor maximum vertical angle in degrees, upward positive
     - -90.0 - 90.0
     - 15.0
   * - <sensor_name>_min_vertical_angle
     - Sensor minimum vertical angle in degrees, upward positive
     - -90.0 - 90.0
     - -15.0
   * - <sensor_name>_vertical_layers
     - Number of vertical layers
     - 2 - 50
     - 5
  


Rover
-----

TODO: ADD image


Rover sensors
`````````````````````````````

.. list-table:: Rover Sensors
   :widths: 1 1 1
   :header-rows: 1

   * - Sensor
     - Name
     - Notes

   * - Day Camera
     - camera
     - Only **camera_hfov** and **camera_gamma** parameters are available in demo version

   * - 2D LIDAR
     - 2d_lidar
     - Not configurable in demo version

   * - 3D LIDAR
     - 2d_lidar
     - Not configurable in demo version

   * - Odometry
     - odometry
     - Not configurable in demo version


Rover Specific Spawn options
`````````````````````````````

.. list-table:: Rover Specific Spawn Options
   :widths: 1 1 1 1
   :header-rows: 1


   * - Option
     - Notes
     - Range
     - Default

   * - enable_2d_lidar
     - Enable 2D laser scanner
     - true \\ false
     - false
   * - enable_3d_lidar
     - Enable 3D laser scanner
     - true \\ false
     - false
   * - enable_odometry
     - Enable odometry publishing
     - true \\ false
     - false



(*) Not all parametersare configurable in demo version


Published topics
```````````````````
#. **/<robot_name>/arena/state - arena_common_interfaces/ArenaPawnState**

  See :ref:`Arena ROS2 Interfaces <arena_ros2_interfaces>`.

#. **/<robot_name>/odom - nav_msgs/Odometry**

  Odometry messages are published if the *enable_odometry* option is enabled.

#. **/<robot_name>/scan - nav_msgs/LaserScan**

  Lase scanner messages are published if the *enable_2d_lidar* option is enabled.


#. **/<robot_name>/scan - nav_msgs/LaserScan**

  2D Lase scanner messages are published if the *enable_2d_lidar* option is enabled.


#. **/<robot_name>/cloud - sensor_msgs/PointCloud2**

  3D Multi layer laser scanner messages are published if the *enable_3d_lidar* option is enabled.


#. **/tf - tf2_msgs/TFMessage**

  TF the following frames are published: odom, base_link, base_footprint, 2d_lidar_link, 3d_lidar_link, camera_link

  If the *use_tf_prefix* option is enabled then a prefix will be added before the frame name.


Subscribed topics
```````````````````

#. **/<robot_name>/cmd_vel - geometry_msgs/Twist**

  (*) Take into account that the Rover robot has an Ackerman-type steering and is therefore unable to make a turn on the spot.

Services
`````````````

#. **/<robot_name>/set_front_lights - std_srvs/SetBool**

  This service is used to turn thr robot front light on and off.


Drone
-----------


TODO: ADD image


Drone sensors
`````````````````````````````

.. list-table:: Drone Sensors
   :widths: 1 1 1
   :header-rows: 1

   * - Sensor
     - Name
     - Notes

   * - Day Camera
     - camera
     - Only **camera_hfov** and **camera_gamma** parameters are available in demo version


Drone Specific Spawn options
`````````````````````````````

.. list-table:: Drone Specific Spawn Options
   :widths: 1 1 1 1
   :header-rows: 1


   * - Option
     - Notes
     - Range
     - Default

   * - self_destroy_timeout
     - The time to wait for the arrival of a */<robot_name>/arena/set_state*  message before self-destructing in the Arena
     - 0.0
     - 0.0 = disables


(*) Not all parametersare configurable in demo version


Published topics
```````````````````

#. **/<robot_name>/arena/state - arena_common_interfaces/ArenaPawnState**

  See :ref:`Arena ROS2 Interfaces <arena_ros2_interfaces>`.

  * The additioanl floats vector first elemnt is used for AGL (above ground level) readings.
  * The additioanl strings vector first elemnt is used for collision detection.

#. **/<robot_name>/gimbal/state - geometry_msgs/Vector3**

  This topic setup and logic is done on the DroneBackend class and not in the Arena. 
  It is used for getting the gimbal inertial roll, pitch and azimuth. See DroneBackend class code for more details. 


Subscribed topics
```````````````````

#. **/<robot_name>/arena/set_state - arena_common_interfaces/ArenaPawnState**

  See :ref:`Arena ROS2 Interfaces <arena_ros2_interfaces>`.
  
  * The additioanl floats vector is used for gimbal direction and פropellers animation.


#. **/<robot_name>/gimbal/command - geometry_msgs/Vector3**

  This topic setup and logic is done on the DroneBackend class and not in the Arena. 
  It is used for Controlling the gimbal inertial roll, pitch and azimuth. See DroneBackend class code for more details. 
