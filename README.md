# Arena Demo Docs Reop


Online docs url:  https://arena-demo-docs.readthedocs.io/en/latest/


## Read the Docs

https://readthedocs.org/projects/arena-demo-docs/



## Setup

```
pip3 install sphinx
pip3 install myst-parser
pip3 install sphinx-rtd-theme

mkdir docs
cd docs
sphinx-quickstart


```

## Local build

```
cd docs

make html
```